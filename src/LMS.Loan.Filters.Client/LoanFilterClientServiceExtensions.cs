﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using System;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LMS.Loan.Filters.Client
{
    public static class LoanFilterClientServiceExtensions
    {
        public static IServiceCollection AddLoanFilterService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<ILoanFilterClientServiceFactory>(p => new LoanFilterClientServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ILoanFilterClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
        public static IServiceCollection AddLoanFilterService(this IServiceCollection services, Uri uri)
        {
            services.AddSingleton<ILoanFilterClientServiceFactory>(p => new LoanFilterClientServiceFactory(p, uri));
            services.AddSingleton(p => p.GetService<ILoanFilterClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddLoanFilterService(this IServiceCollection services)
        {
            services.AddSingleton<ILoanFilterClientServiceFactory>(p => new LoanFilterClientServiceFactory(p));
            services.AddSingleton(p => p.GetService<ILoanFilterClientServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

    }
}
