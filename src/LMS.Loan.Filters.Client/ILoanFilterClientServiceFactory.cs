﻿using LendFoundry.Security.Tokens;
using LMS.Loan.Filters.Abstractions;

namespace LMS.Loan.Filters.Client
{
    public interface ILoanFilterClientServiceFactory
    {
        ILoanFilterService Create(ITokenReader reader);
    }
}
