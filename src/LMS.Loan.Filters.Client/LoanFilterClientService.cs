﻿using RestSharp;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System;
using LMS.Loan.Filters.Abstractions;
using LendFoundry.Foundation.Client;

namespace LMS.Loan.Filters.Client
{
    public class LoanFilterClientService : ILoanFilterService
    {
        public LoanFilterClientService(IServiceClient client) { Client = client; }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAll()
        {
            var request = new RestRequest("/all", Method.GET);

            var loans = Client.ExecuteAsync<List<FilterView>>(request).Result;
            return new List<IFilterView>(loans);
        }

        public async Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses)
        {
            var request = new RestRequest("status/{statuses}", Method.GET);
            var strBuilder = new StringBuilder();
            foreach (var item in statuses) strBuilder.Append($"{item}/");
            request.AddUrlSegment("statuses", strBuilder.ToString());

            return await Client.ExecuteAsync<List<FilterView>>(request);
        }

        public Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public IFilterViewTags GetApplicationTagInformation(string applicationNumber)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetCountByTag(IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public Task<IFilterView> GetFilterView(string loanNumber)
        {
            throw new NotImplementedException();
        }

        public async Task<ILoanMetrics> GetMetrics(string loanNumber)
        {
            var request = new RestRequest("matrics/{loanNumber}", Method.GET);
            request.AddUrlSegment(nameof(loanNumber), loanNumber);
            return await Client.ExecuteAsync<LoanMetrics>(request);
        }

        public Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag)
        {
            throw new NotImplementedException();
        }

        public void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ILoanMetrics>> GetAllMetrics()
        {
            var request = new RestRequest("/all/matrics", Method.GET);

            var loanMetrics = await Client.ExecuteAsync<List<LoanMetrics>>(request);
            return new List<ILoanMetrics>(loanMetrics);
        }

        public async Task<IEnumerable<IFilterView>> GetLoansByProductId(string productid)
        {
            var request = new RestRequest("/loan/{productid}", Method.GET);
            request.AddUrlSegment(nameof(productid), productid);
            var filterViews = await Client.ExecuteAsync<List<FilterView>>(request);
            return new List<IFilterView>(filterViews);
        }

        public async Task<List<IFilterView>> GetLoansByLoanNumbers(List<string> loanNumbers)
        {
            var request = new RestRequest("/loans/loannumbers", Method.POST);

            request.AddJsonBody(loanNumbers);
            var fundDetails = await Client.ExecuteAsync<List<FilterView>>(request);
            return new List<IFilterView>(fundDetails);
        }

    public Task<List<CollectionResponse>> GetCollectionData(LoanCollectionRequest loanCollectionRequest)
    {
      throw new NotImplementedException();
    }

      public Task<List<CollectionResponse>> GetFundingData(LoanCollectionRequest loanCollectionRequest)
    {
      throw new NotImplementedException();
    }

    public Task<List<CollectionResponse>> GetFollowUpData(LoanCollectionRequest loanCollectionRequest)
    {
      throw new NotImplementedException();
    }

        public async Task<List<IFilterView>> GetByBorrowerId(string borrowerId)
        {
            var request = new RestRequest("/borrower/{borrowerId}", Method.GET);
            request.AddUrlSegment(nameof(borrowerId), borrowerId);
            return new List<IFilterView>(await Client.ExecuteAsync<List<FilterView>>(request));
        }

        public async Task<IEnumerable<IFilterView>> GetLoansByOnBoardedByDate(int year, int month, int day)
        {
            var request = new RestRequest("/loans/on-boarded/{year}/{month}/{day}", Method.GET);
            request.AddUrlSegment(nameof(year), year);
            request.AddUrlSegment(nameof(month), month);
            request.AddUrlSegment(nameof(day), day);
            return new List<IFilterView>(await Client.ExecuteAsync<List<FilterView>>(request));
        }
        
    }
}
