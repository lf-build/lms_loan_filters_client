﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using LendFoundry.ProductRule;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace LMS.Loan.Filters.Abstractions
{
    public interface ILoanMetrics : IAggregate
    {
        string LoanNumber { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IMetricsDetail, MetricsDetail>))]
        List<IMetricsDetail> Metrics { get; set; }
    }
}