﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;
using Newtonsoft.Json;
using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using LendFoundry.StatusManagement;
using LMS.LoanAccounting;
using LMS.Loan.Filters.Abstractions;

namespace LMS.Loan.Filters
{
    public interface IFilterView : IAggregate
    {
        string LoanNumber { get; set; }
        TimeBucket LoanOnboardedDate { get; set; }
        TimeBucket LoanFundedDate { get; set; }
        TimeBucket LoanStartDate { get; set; }
        TimeBucket LoanEndDate { get; set; }
        string LoanApplicationNumber { get; set; }
        string LoanProductId { get; set; }
        string ProductPortfolioType { get; set; }
        string ProductCategory { get; set; }
        string PrimaryApplicantAddressLine1 { get; set; }
        string PrimaryApplicantAddressLine2 { get; set; }
        string PrimaryApplicantCity { get; set; }
        string PrimaryApplicantState { get; set; }
        string PrimaryApplicantZip { get; set; }
        string PrimaryApplicantEmailAddress { get; set; }
        string PrimaryApplicantFirstName { get; set; }
        string PrimaryApplicantGeneration { get; set; }
        string PrimaryApplicantLastName { get; set; }
        string PrimaryApplicantMiddle { get; set; }
        string PrimaryApplicantSalutation { get; set; }
        string PrimaryApplicantPhoneNo { get; set; }
        string SecondaryApplicantPhoneNo { get; set; }
        string PrimaryApplicantDOB { get; set; }
        string PrimaryApplicantSSN { get; set; }
        string PrimaryApplicantPositionOfLoan { get; set; }
        string PrimaryApplicantRole { get; set; }
        string PrimaryBusinessAddressLine1 { get; set; }
        string PrimaryBusinessAddressLine2 { get; set; }
        string PrimaryBusinessCity { get; set; }
        string PrimaryBusinessState { get; set; }
        string PrimaryBusinessZip { get; set; }
        string PrimaryBusinessPhoneNo { get; set; }
        string PrimaryBusinessEmailAddress { get; set; }
        string PrimaryBusinessName { get; set; }
        string PrimaryBusinessDBA { get; set; }
        string PrimaryBusinessFedTaxId { get; set; }
        string CurrentLoanScheduleNumber { get; set; }
        string CurrentLoanScheduleVersion { get; set; }
        double LoanAmount { get; set; }
        double Fundedamount { get; set; }
        TimeBucket FundedDate { get; set; }
        TimeBucket FirstPaymentDate { get; set; }
        string PaymentFrequency { get; set; }
        double FrequencyRate { get; set; }
        double DailyRate { get; set; }
        double AnnualRate { get; set; }
        int Tenure { get; set; }
         string LoanModReason { get; set; }
        double FactorRate { get; set; }
        string StatusCode { get; set; }
        IList<string> Reasons { get; set; }
        TimeBucket StatusDate { get; set; }
        string StatusName { get; set; }
        bool IsAutoPay { get; set; }
        TimeBucket AutoPayStartDate { get; set; }
        TimeBucket AutoPayStopDate { get; set; }

        string StatusWorkFlowId { get; set; }

        WorkFlowStatus WorkFlowStatus { get; set; }

        List<ISubStatus> SubStatusDetail { get; set; }

        double PrincipalAmount { get; set; }
        bool AccrualStop { get; set; }
        TimeBucket AccrualStopDate { get; set; }
        TimeBucket ProcessingDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPBOC, PBOC>))]
        IPBOC PBOC { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPBOT, PBOT>))]
        IPBOT PBOT { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPaymentInfo, PaymentInfo>))]
        IPaymentInfo PaymentInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDue, Due>))]
        IDue Due { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IChargeOff, ChargeOff>))]
        IChargeOff ChargeOff { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPayOff, PayOff>))]
        IPayOff PayOff { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISellOff, SellOff>))]
        ISellOff SellOff { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IExcessMoney, ExcessMoney>))]
        IExcessMoney ExcessMoney { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IPaymentDetails, PaymentDetails>))]
        List<IPaymentDetails> PaymentDetails { get; set; }
        double CreditLimit { get; set; }
        int NoOfActiveFlgas { get; set; }
        List<CollectionDetails> CollectionDetails { get; set; }
        int NumberOfPaymentMade { get; set; }
        Dictionary<string,string> Personalidentifiers{ get; set;}
        Dictionary<string,string> BusinessIdentifiers{ get; set;}
        string BorrowerId {get;set;}
        double BusinessMaxLimitAmount{get;set;}
        string FunderId { get; set; }
        string AdditionalRefId { get; set; }
        string AggregatorName { get; set; }
        string Beneficiary { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPriorInterest, PriorInterest>))]
        IPriorInterest PriorInterest { get; set; }
    }
}