﻿using System;
using System.Collections.Generic;

namespace LMS.Loan.Filters.Abstractions
{
    public class TagResult
    {
        public List<string> ApplyTags { get; set; }
        public List<string> RemoveTags { get; set; }
    }
}
