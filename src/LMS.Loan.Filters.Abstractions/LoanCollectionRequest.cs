﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Filters
{
    public class LoanCollectionRequest
    {
        public DateTimeOffset FromDate { get; set; }

        public DateTimeOffset ToDate { get; set; }
        public DateTimeOffset? LastFromDate { get; set; }

        public DateTimeOffset? LastToDate { get; set; }
        public string LastCallDesposition{ get; set; }


    }
}
