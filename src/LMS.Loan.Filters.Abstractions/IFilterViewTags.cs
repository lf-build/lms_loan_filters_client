﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LMS.Loan.Filters.Abstractions
{
    public interface IFilterViewTags : IAggregate
    {
        string LoanNumber { get; set; }
        IEnumerable<TagInfo> Tags { get; set; }
    }
}
