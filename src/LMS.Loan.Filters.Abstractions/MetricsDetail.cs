﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Abstractions
{
    public class MetricsDetail : IMetricsDetail
    {
        public string Name { set; get; }
        public int Value { get; set; }
    }
}
