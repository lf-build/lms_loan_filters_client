#if DOTNET2
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Abstractions
{
    public interface ILoanFilterService
    {
        IEnumerable<IFilterView> GetAll();
        Task<IFilterView> GetFilterView(string loanNumber);
        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);
        Task<IEnumerable<IFilterView>> GetAllByTag(IEnumerable<string> tags);
        Task<int> GetCountByTag(IEnumerable<string> tags);
        Task<IEnumerable<IFilterViewTags>> GetTagInfoForApplicationsByTag(string tag);
        void TagsWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);
        void UnTagWithoutEvaluation(string applicationNumber, IEnumerable<string> tags);
        IFilterViewTags GetApplicationTagInformation(string applicationNumber);
        Task<ILoanMetrics> GetMetrics(string loanNumber);
        Task<List<ILoanMetrics>> GetAllMetrics();   
        Task<IEnumerable<IFilterView>> GetLoansByProductId(string productid);
        Task<List<IFilterView>> GetLoansByLoanNumbers(List<string> loanNumbers);
        Task<List<CollectionResponse>> GetCollectionData(LoanCollectionRequest loanCollectionRequest);
        Task<List<CollectionResponse>> GetFundingData(LoanCollectionRequest loanCollectionRequest);
        Task<List<CollectionResponse>> GetFollowUpData(LoanCollectionRequest loanCollectionRequest);
        Task<List<IFilterView>> GetByBorrowerId(string borrowerId);
        Task<IEnumerable<IFilterView>> GetLoansByOnBoardedByDate(int year,int month, int day);


    }
}
