﻿namespace LMS.Loan.Filters.Abstractions
{
    public interface IEventsForTaggingListener
    {
        void Start();
    }
}
