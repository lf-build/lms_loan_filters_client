﻿using LendFoundry.EventHub;
namespace LMS.Loan.Filters.Abstractions
{
    public interface ILoanFilterServiceExtended : ILoanFilterService
    {
        void ProcessTaggingEvent(EventInfo @event);

    }
}
