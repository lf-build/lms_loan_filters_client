﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Abstractions
{
     public interface ILoanMetricsRepositoryFactory
    {
        ILoanMetricsRepository Create(ITokenReader reader);
    }
}
