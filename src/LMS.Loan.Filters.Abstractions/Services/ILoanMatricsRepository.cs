﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Abstractions
{
    public interface ILoanMetricsRepository : IRepository<ILoanMetrics>
    {
        Task AddOrUpdate(ILoanMetrics metrics);
        Task<ILoanMetrics> GetMetrics(string LoanNumber);
        Task<List<ILoanMetrics>> GetAllMetrics();
    }
}