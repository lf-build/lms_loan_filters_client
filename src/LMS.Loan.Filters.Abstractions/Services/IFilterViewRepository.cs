﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Abstractions
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        IEnumerable<IFilterView> GetAll();
        Task<IFilterView> GetFilterView(string loanNumber);
        void AddOrUpdate(IFilterView view);
        Task<IEnumerable<IFilterView>> GetAllByStatus(IEnumerable<string> statuses);
        Task<IEnumerable<IFilterView>> GetLoansByProductId(string productid);
        Task<List<IFilterView>> GetLoansByLoanNumbers(List<string> loanNumbers);
        Task<List<IFilterView>> GetCollectionData(LoanCollectionRequest loanCollectionRequest);
        Task<List<IFilterView>> GetFundingData(LoanCollectionRequest loanCollectionRequest);

        Task<List<IFilterView>> GetByBorrowerId(string borrowerId);
        Task<IEnumerable<IFilterView>> GetLoansByOnBoardedByDate(int year,int month, int day);
    }
}