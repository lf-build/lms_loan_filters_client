﻿using LendFoundry.Security.Tokens;

namespace LMS.Loan.Filters.Abstractions
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}
