﻿using LendFoundry.Security.Tokens;

namespace LMS.Loan.Filters.Abstractions
{
    public interface ITagFilterRepositoryFactory
    {
        ITagFilterRepository Create(ITokenReader reader);
    }
}
