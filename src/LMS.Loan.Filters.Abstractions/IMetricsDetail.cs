﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Abstractions
{
    public interface IMetricsDetail
    {
         string Name { set; get; }
         int Value { get; set; }
    }
}
