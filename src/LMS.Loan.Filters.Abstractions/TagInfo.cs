﻿using LendFoundry.Foundation.Date;
using System;

namespace LMS.Loan.Filters.Abstractions
{
    public class TagInfo
    {
        public string TagName { get; set; }
        public TimeBucket TaggedOn { get; set; }
    }
}
