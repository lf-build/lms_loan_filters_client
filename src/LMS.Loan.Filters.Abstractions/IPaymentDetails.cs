﻿using LendFoundry.Foundation.Date;

namespace LMS.Loan.Filters.Abstractions
{
    public interface IPaymentDetails
    {
        string ReferenceId { get; set; }
        TimeBucket ActivityDate { get; set; }
        string ActivityStatus { get; set; }
        int InstallmentNo { get; set; }
        double PaymentAmount { get; set; }
        TimeBucket ScheduleDate { get; set; }
        string ReturnCode { get; set; }
        TimeBucket ReturnDate { get; set; }
        string ReturnReason { get; set; }
    }
}