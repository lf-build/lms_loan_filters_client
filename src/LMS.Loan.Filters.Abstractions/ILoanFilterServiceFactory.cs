﻿
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;

namespace LMS.Loan.Filters.Abstractions
{
    public interface ILoanFilterServiceFactory
    {
        ILoanFilterServiceExtended Create(ITokenReader reader, ILogger logger, ITokenHandler tokenHandler);
    }
}
