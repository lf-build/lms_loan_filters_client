﻿using System.Collections.Generic;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.StatusManagement;
using LMS.LoanAccounting;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using LMS.Loan.Filters.Abstractions;

namespace LMS.Loan.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public string LoanNumber { get; set; }
        public TimeBucket LoanOnboardedDate { get; set; }
        public TimeBucket LoanFundedDate { get; set; }
        public TimeBucket LoanStartDate { get; set; }
        public TimeBucket LoanEndDate { get; set; }
        public string LoanApplicationNumber { get; set; }
        public string LoanProductId { get; set; }
        public string ProductPortfolioType { get; set; }
        public string ProductCategory { get; set; }
        public IList<string> Reasons { get; set; }
        public string PrimaryApplicantAddressLine1 { get; set; }
        public string PrimaryApplicantAddressLine2 { get; set; }
        public string PrimaryApplicantCity { get; set; }
        public string PrimaryApplicantState { get; set; }
        public string PrimaryApplicantZip { get; set; }
        public string PrimaryApplicantEmailAddress { get; set; }
        public string PrimaryApplicantFirstName { get; set; }
        public string PrimaryApplicantGeneration { get; set; }
        public string PrimaryApplicantLastName { get; set; }
        public string PrimaryApplicantMiddle { get; set; }
        public string PrimaryApplicantSalutation { get; set; }
        public string PrimaryApplicantPhoneNo { get; set; }
         public string SecondaryApplicantPhoneNo { get; set; }

        public string PrimaryApplicantDOB { get; set; }
        public string PrimaryApplicantSSN { get; set; }
        public string PrimaryApplicantPositionOfLoan { get; set; }
        public string PrimaryApplicantRole { get; set; }
        public string PrimaryBusinessAddressLine1 { get; set; }
        public string PrimaryBusinessAddressLine2 { get; set; }
        public string PrimaryBusinessCity { get; set; }
        public string PrimaryBusinessState { get; set; }
        public string PrimaryBusinessZip { get; set; }
        public string PrimaryBusinessPhoneNo { get; set; }
        public string PrimaryBusinessEmailAddress { get; set; }
        public string PrimaryBusinessName { get; set; }
        public string PrimaryBusinessDBA { get; set; }
        public string PrimaryBusinessFedTaxId { get; set; }
        public string CurrentLoanScheduleNumber { get; set; }
        public string CurrentLoanScheduleVersion { get; set; }
        public double LoanAmount { get; set; }
        public double Fundedamount { get; set; }
        public TimeBucket FundedDate { get; set; }
        public TimeBucket FirstPaymentDate { get; set; }
        public string PaymentFrequency { get; set; }
        public double FrequencyRate { get; set; }
        public double DailyRate { get; set; }
        public double AnnualRate { get; set; }
        public int Tenure { get; set; }
        public double FactorRate { get; set; }
        public string StatusCode { get; set; }
        public string LoanModReason { get; set; }
        public TimeBucket StatusDate { get; set; }
        public string StatusName { get; set; }
        public bool IsAutoPay { get; set; }
        public TimeBucket AutoPayStartDate { get; set; }
        public TimeBucket AutoPayStopDate { get; set; }
        public double CreditLimit { get; set; }

        public string StatusWorkFlowId { get; set; }
        public WorkFlowStatus WorkFlowStatus { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<ISubStatus, SubStatus>))]
        public List<ISubStatus> SubStatusDetail { get; set; }

        public double PrincipalAmount { get; set; }
        public bool AccrualStop { get; set; }
        public TimeBucket AccrualStopDate { get; set; }
        public TimeBucket ProcessingDate { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPBOC, PBOC>))]
        public IPBOC PBOC { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPBOT, PBOT>))]
        public IPBOT PBOT { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPaymentInfo, PaymentInfo>))]
        public IPaymentInfo PaymentInfo { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IDue, Due>))]
        public IDue Due { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IChargeOff, ChargeOff>))]
        public IChargeOff ChargeOff { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPayOff, PayOff>))]
        public IPayOff PayOff { get; set; }
        [JsonConverter(typeof(InterfaceConverter<ISellOff, SellOff>))]
        public ISellOff SellOff { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IExcessMoney, ExcessMoney>))]
        public IExcessMoney ExcessMoney { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IPaymentDetails, PaymentDetails>))]
        public List<IPaymentDetails> PaymentDetails { get; set; }
        public int NoOfActiveFlgas { get; set; }
        public List<CollectionDetails> CollectionDetails { get; set; }
        public int NumberOfPaymentMade { get; set; }

        public Dictionary<string,string> Personalidentifiers{ get; set;}
        public Dictionary<string,string> BusinessIdentifiers{ get; set;}
        public string BorrowerId {get;set;}
        public double BusinessMaxLimitAmount{get;set;}
        public string FunderId { get; set; }
        public string AdditionalRefId { get; set; }
        public string AggregatorName { get; set; }
        public string Beneficiary { get; set; }
        
        [JsonConverter(typeof(InterfaceConverter<IPriorInterest, PriorInterest>))]
        public IPriorInterest PriorInterest { get; set; }
    }
}