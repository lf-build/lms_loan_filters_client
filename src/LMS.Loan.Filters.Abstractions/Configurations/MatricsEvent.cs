﻿using System;
using System.Collections.Generic;

namespace LMS.Loan.Filters.Abstractions.Configurations
{
    public class MetricsEvent
    {
        public string Name { get; set; }
        public List<string> MetricsToUpdte { get; set; }
        public bool IsResetCounter { get; set; }
         public bool IsEventToBePublish { get; set; }
        public string Counter { get; set; }
    }
}
