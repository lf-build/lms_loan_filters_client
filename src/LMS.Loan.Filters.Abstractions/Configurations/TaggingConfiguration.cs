﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LMS.Loan.Filters.Abstractions.Configurations
{
    public class TaggingConfiguration : IDependencyConfiguration
    {
        public List<TaggingEvent> Events { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
    }
}
