﻿namespace LMS.Loan.Filters.Abstractions.Configurations
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string LoanNumber { get; set; }
        public bool ShouldUpdateAppFilterRecord { get; set; }
        public bool ShouldUpdateCollectionRecord { get; set; }
        public bool ShouldUpdateFlagCount { get; set; }
        public string Response { get; set; }
    }
}
