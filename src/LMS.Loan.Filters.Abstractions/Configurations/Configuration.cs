﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace LMS.Loan.Filters.Abstractions.Configurations
{
    public class Configuration :IDependencyConfiguration
    {
        public EventMapping[] Events { get; set; }
        public string[] ApprovedStatuses { get; set; }
     
        public List<MetricsEvent> MetricsEvent { get; set; }

        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }
        public string ConnectionString { get; set; }


    }
}
