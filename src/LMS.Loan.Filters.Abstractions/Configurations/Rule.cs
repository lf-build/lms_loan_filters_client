﻿namespace LMS.Loan.Filters.Abstractions.Configurations
{
    public class Rule
    {
        public string Name { get; set; }
        public string Version { get; set; }
    }

}
