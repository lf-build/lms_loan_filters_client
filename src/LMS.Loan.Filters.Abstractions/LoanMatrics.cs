﻿using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using LendFoundry.ProductRule;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LMS.Loan.Filters.Abstractions
{
    public class LoanMetrics : Aggregate, ILoanMetrics
    {
        public string LoanNumber { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IMetricsDetail, MetricsDetail>))]
        public List<IMetricsDetail> Metrics { get; set; }

    }
}
