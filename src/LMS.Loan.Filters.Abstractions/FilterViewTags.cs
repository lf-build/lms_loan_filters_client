﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LMS.Loan.Filters.Abstractions
{
    public class FilterViewTags : Aggregate, IFilterViewTags, IEquatable<FilterViewTags>
    {
        public FilterViewTags(string entityId, IEnumerable<TagInfo> tags)
        {
            LoanNumber = entityId;
            Tags = tags;
        }
        public string LoanNumber { get; set; }

        public string EntityType { get; set; }
       
        public IEnumerable<TagInfo> Tags { get; set; }

        public bool Equals(FilterViewTags other)
        {
            return other == null ? false : this.LoanNumber == other.LoanNumber && this.TenantId == other.TenantId;
        }
    }
}
