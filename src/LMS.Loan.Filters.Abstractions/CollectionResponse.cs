﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;

namespace LMS.Loan.Filters.Abstractions
{
    public class CollectionResponse
    {
        public string LoanNumber { set; get; }
        public string LoanProductId { get; set; }
        public string StatusWorkFlowId { get; set; }
        public string BorrowerName{ get; set; }
        public string PrimaryPhoneNumber{ get; set; }
        public string SecondaryPhoneNumber{ get; set; }
        public double FundedAmount{ get; set; }
        public double RepaymentAmount{ get; set; }
        public string PreviousCollectionStage{ get; set; }
        public string CurrentCollectionStage{ get; set; }
        public TimeBucket DateOfTransition{ get; set; }
        public int TotalNumberOfPaymentMade{ get; set; }
        public double TotalAmountReceived{ get; set; }
        public string ReasonOfTransition{ get; set; }
        public double TotalOutstandingAmount { get; set; }
        public double TotalOutstandingPercentage { get; set; }
        public string LoanStatus{get;set;}
        public TimeBucket LastCallDate{ get; set; }
        public TimeBucket LastSuccessfulContactedDate{ get; set; }
        public TimeBucket FollowUpDate { get; set; }
        public  string LastCallDisposition { get; set; }
        public string ProductPortfolioType{ get; set; }
        public string FunderId{get;set;}
        public string FunderName{get;set;}
        public string BorrowerId {get;set;}
        public double BusinessMaxLimitAmount{get;set;}
        public string ProductName {get;set;}

    }
}
