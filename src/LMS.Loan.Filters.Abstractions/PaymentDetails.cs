﻿using LendFoundry.Foundation.Date;

namespace LMS.Loan.Filters.Abstractions
{
    public class PaymentDetails : IPaymentDetails
    {
        public string ReferenceId { get; set; }
        public TimeBucket ActivityDate { get; set; }  
        public TimeBucket ScheduleDate { get; set; }  
        public int InstallmentNo { get; set; }   
        public double PaymentAmount { get; set; }  
        public string ActivityStatus { get; set; }
        public string ReturnCode { get; set; }
        public TimeBucket ReturnDate { get; set; }
        public string ReturnReason { get; set; }

    }
}
