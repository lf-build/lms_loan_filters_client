using System;

namespace LMS.Loan.Filters{
    public class Settings {
        // public static string ServiceName { get; } = "loan-filters";

         public static string TaggingEvents { get; } = "tagging-events";
        public static string ServiceName => Environment.GetEnvironmentVariable ($"CONFIGURATION_NAME") ?? "loan-filters";
    }
}