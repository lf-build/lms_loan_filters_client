﻿using LendFoundry.Foundation.Date;

namespace LMS.Loan.Filters.Abstractions
{
    public class CollectionDetails 
    {
        public string OldCollectionStage { get; set; }
        public TimeBucket DateOfTransition { get; set; }  
        public string CurrentCollectionStage { get; set; }  
        public string ReasonOfTransition { get; set; }

    }
}
